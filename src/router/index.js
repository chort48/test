import VueRouter from 'vue-router'

import District from "@/components/District/District";

export default new VueRouter({
    routes:[
        {
            path: '/District',
            component: District
        }
    ],
    mode: 'history'
})